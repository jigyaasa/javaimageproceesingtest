package com.ImageProcessTest.ImageProcessTest;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Hello world!
 *
 */
public class App 
{
	private static BufferedImage rgb2gray(int width,int height,BufferedImage image) {

		// convert to greyscale 
        for (int y = 0; y < height; y++) 
        { 
            for (int x = 0; x < width; x++) 
            { 
                // Here (x,y)denotes the coordinate of image  
                // for modifying the pixel value. 
                int p = image.getRGB(x,y); 
  
                int a = (p>>24)&0xff; 
                int r = (p>>16)&0xff; 
                int g = (p>>8)&0xff; 
                int b = p&0xff; 
  
                // calculate average 
                int avg = (r+g+b)/3; 
//                if(r == 0 && g ==0 && b == 0) {
//	                r = 255;
//	                g = 0;
//	                b = 0;
//                }
//                p = (a<<24) | (r<<16) | (g<<8) | b;
                // replace RGB value with avg 
                p = (a<<24) | (avg<<16) | (avg<<8) | avg; 
  
                image.setRGB(x, y, p); 
            } 
        } 		
		return image;
	}
	
	private static BufferedImage im2bw(int width, int height, BufferedImage image) {
		// convert to greyscale 
		long grayThreshold = 0x999999;
        for (int y = 0; y < height; y++) 
        { 
            for (int x = 0; x < width; x++) 
            { 
                // Here (x,y)denotes the coordinate of image  
                // for modifying the pixel value. 
                int p = image.getRGB(x,y); 
  
                int a = (p>>24)&0xff; 
//                int r = (p>>16)&0xff; 
//                int g = (p>>8)&0xff; 
//                int b = p&0xff; 
  
                // calculate average 
                if((p & 0xffffff) > grayThreshold) {
                   p = (a<<24) & 0xff000000; 
                }
                else {
                	p = (a<<24) | 0x00ffffff;
                }
//                p = (a<<24) | (r<<16) | (g<<8) | b;
                // replace RGB value with avg 
//                p = (a<<24) | (avg<<16) | (avg<<8) | avg; 
  
                image.setRGB(x, y, p); 
            } 
        } 		
		return image;
	}	

	private static double compareImages(BufferedImage image, int width, int height, BufferedImage db_image,
			int db_width, int db_height) {
		// TODO Auto-generated method stub
         
		double percentage = 100;
        if ((width != db_width) || (height != db_height)) 
            System.out.println("Error: Images dimensions"+ 
                                             " mismatch"); 
        else
        { 
            long difference = 0; 
            for (int y = 0; y < height; y++) 
            { 
                for (int x = 0; x < width; x++) 
                { 
                    int pA = image.getRGB(x, y); 
                    int pB = db_image.getRGB(x, y); 
                    int redA = (pA >> 16) & 0xff; 
                    int greenA = (pA >> 8) & 0xff; 
                    int blueA = (pA) & 0xff; 
                    int redB = (pB >> 16) & 0xff; 
                    int greenB = (pB >> 8) & 0xff; 
                    int blueB = (pB) & 0xff; 
                    difference += Math.abs(redA - redB); 
                    difference += Math.abs(greenA - greenB); 
                    difference += Math.abs(blueA - blueB); 
                } 
            } 
  
            // Total number of red pixels = width * height 
            // Total number of blue pixels = width * height 
            // Total number of green pixels = width * height 
            // So total number of pixels = width * height * 3 
            double total_pixels = width * height * 3; 
  
            // Normalizing the value of different pixels 
            // for accuracy(average pixels per color 
            // component) 
            double avg_different_pixels = difference / 
                                          total_pixels; 
  
            // There are 255 values of pixels in total 
            percentage = (avg_different_pixels / 
                                            255) * 100; 
  
        }
		
		return percentage;
	}
	
	
	public static void main( String[] args )
    {
        int width; //= 963;    //width of the image 
        int height; //= 640;   //height of the image 
        int db_width; //= 963;    //width of the image 
        int db_height; //= 640;   //height of the image 
  
        // For storing image in RAM 
        BufferedImage image = null; 
        BufferedImage db_image = null;
        // READ IMAGE 
        try
        { 
            File input_file = new File("src/main/resources/Signature1 - Copy.jpg"); //image file path 
            File db_file = new File("src/main/resources/SignatureDB.jpg"); //db_image file path  
            /* create an object of BufferedImage type and pass 
               as parameter the width,  height and image int 
               type.TYPE_INT_ARGB means that we are representing 
               the Alpha, Red, Green and Blue component of the 
               image pixel using 8 bit integer value. */
//            image = new BufferedImage(width, height, 
//                                    BufferedImage.TYPE_INT_ARGB); 
  
             // Reading input file 
            image = ImageIO.read(input_file);
            db_image = ImageIO.read(db_file);
            width = image.getWidth();
            height = image.getHeight();
            db_width = db_image.getWidth();
            db_height = db_image.getHeight();
            image = rgb2gray(width,height,image);
            image = im2bw(width,height,image);
            double percentage = compareImages(image,width,height,db_image,db_width,db_height);
            System.out.println("Difference Percentage-->" + 
                    percentage);	

            System.out.println("Reading complete."); 
        } 
        catch(IOException e) 
        { 
            System.out.println("Error: "+e);
            e.printStackTrace();
        } 
  
        // WRITE IMAGE 
//        try
//        { 
//            // Output file path 
//            File output_file = new File("src/main/resources/Out.jpg"); 
//  
//            // Writing to file taking type and path as 
//            ImageIO.write(image, "jpg", output_file); 
//  
//            System.out.println("Writing complete."); 
//        } 
//        catch(IOException e) 
//        { 
//            System.out.println("Error: "+e); 
//        }     	
        System.out.println( "Hello World!" );
    }


}
